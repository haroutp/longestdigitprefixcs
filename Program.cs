﻿using System;

namespace LongestDigitPrefix
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        string longestDigitsPrefix(string inputString) {
            var characters = inputString.ToCharArray();
            var longestDigitCount = "";
            
            foreach(char item in characters){
                if(char.IsDigit(item)){
                    longestDigitCount += item;
                }else{
                    return longestDigitCount;
                }
            }
            return longestDigitCount;
        }

    }
}
